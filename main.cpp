#include "app.cpp"

#include <cstdlib>

int main() {
    VulkanApp app;
    VulkanApp app2 = app;

    try {
        app.run();
    }
    catch(const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
